package com.aixiao.seckill.service.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aixiao.entity.seckill.Order;
import com.aixiao.seckill.mapper.OrderMapper;
import com.aixiao.seckill.service.OrderService;

/**
 * @author fan shi ke
 * @date 2019年3月20日 下午5:26:02
 * @Descripton
 */
@Service
public class OrderServiceImpl implements OrderService {

	

	@Autowired
	private OrderMapper orderMapper;
	Scanner input = new Scanner(System.in);
	public Order getById(Integer id) {
		return orderMapper.getById(id);
	}

	public synchronized void insertOrder(Integer userId, Integer goodsId, Double money, Long iphone) {
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", userId);
		paramMap.put("goodsId", goodsId);
		paramMap.put("money", money);
		paramMap.put("nowTime", new Timestamp(new Date().getTime()));
		paramMap.put("iphone", iphone);
		paramMap.put("result", -1);
		orderMapper.executeSeckillPro(paramMap);
		Integer result = (Integer) paramMap.get("result");
		try {
			if (result == 0) {
				throw new Exception("活动还未开始或者已结束，或者商品号：" + goodsId + " 库存不足");
			}else if(result == -1){
				throw new Exception("服务错误");
			}else if(result == 1){
				System.out.println("商品号："+goodsId+" 购买成功");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}finally{
			System.out.print("请继续购买：");
		}
	}

}

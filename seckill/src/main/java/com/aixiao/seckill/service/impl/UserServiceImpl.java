package com.aixiao.seckill.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aixiao.entity.seckill.User;
import com.aixiao.seckill.mapper.UserMapper;
import com.aixiao.seckill.service.UserService;

/**
 * @author fan shi ke
 * @date 2019年3月20日 下午5:26:02
 * @Descripton
 */
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper userMapper;
	
	public User getById(Integer id) {
		return userMapper.getById(id);
	}
	
	@Override
	public List<Map<String, Object>> queryAll() {
		// TODO Auto-generated method stub
		return userMapper.queryAll();
	}
	
	public User findByName(String name){
		return userMapper.getByName(name);
	}

	/* (non-Javadoc)
	 * @see com.aixiao.seckill.service.UserService#login(com.aixiao.entity.seckill.User)
	 */
	@Override
	public User login(User user) {
		return userMapper.getUserByNameAndPassWord(user);
	}

	

}

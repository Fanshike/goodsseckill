package com.aixiao.seckill.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aixiao.entity.seckill.Permission;
import com.aixiao.seckill.mapper.PermissionMapper;
import com.aixiao.seckill.service.PermissionService;

/**
 * @author fan shi ke
 * @date 2019年3月20日 下午5:26:02
 * @Descripton
 */
@Service
public class PermissionServiceImpl implements PermissionService {

	@Autowired
	private PermissionMapper permissionMapper;
	Scanner input = new Scanner(System.in);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aixiao.seckill.service.OrderService#getById(java.lang.Integer)
	 */
	public List<Map<String,Object>> queryAll(Integer page) {
		page = page <= 0 ? 1 : page;
		int pageIndex = (page - 1) * 10;
		return  permissionMapper.queryAll(pageIndex);
		
	}

	/*
	 * 获取一共多少页数据
	 */
	public Long getAllSize() {
		Long allSize = permissionMapper.getAllSize();
		long pageSize = Math.floorMod(allSize, 10);
		pageSize = pageSize != 0 ? allSize / (long) 10 + 1 : allSize / (long) 10;
		return pageSize;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aixiao.seckill.service.GoodsService#getById()
	 */
	public Permission getById(Integer id) {
		Permission permission = permissionMapper.getById(id);
		return permission;
	}

	/* (non-Javadoc)
	 * @see com.aixiao.seckill.service.PermissionService#getByRoleId(java.lang.Integer)
	 */
	@Override
	public List<Map<String, Object>> getByRoleId(Integer roleId) {
		return permissionMapper.getByRoleId(roleId);
	}

}

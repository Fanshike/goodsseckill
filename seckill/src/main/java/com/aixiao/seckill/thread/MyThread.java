package com.aixiao.seckill.thread;

import org.springframework.stereotype.Component;

import com.aixiao.seckill.service.impl.OrderServiceImpl;

@Component
public class MyThread implements Runnable {

	private Integer userId;
	private Integer goodsId;
	private Double money;
	private Long iphone;

	public MyThread(Integer userId, Integer goodsId, Double money, Long iphone, OrderServiceImpl orderServiceImpl) {
		super();
		this.userId = userId;
		this.goodsId = goodsId;
		this.money = money;
		this.iphone = iphone;
		this.orderServiceImpl = orderServiceImpl;
	}

	public MyThread() {
	}

	private OrderServiceImpl orderServiceImpl;

	public void run() {
		orderServiceImpl.insertOrder(this.userId, this.goodsId, this.money, this.iphone);
	}
}

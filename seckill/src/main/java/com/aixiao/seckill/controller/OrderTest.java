package com.aixiao.seckill.controller;

import java.util.Collection;
import java.util.Date;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.subject.Subject;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.aixiao.entity.seckill.Order;
import com.aixiao.entity.seckill.User;
import com.aixiao.seckill.service.impl.OrderServiceImpl;
import com.aixiao.seckill.service.impl.UserServiceImpl;
import com.aixiao.shiro.util.JsonResult;
import com.aixiao.shiro.util.Msg;

/**
 * @author fan shi ke
 * @date 2019年3月20日 下午5:58:57
 * @Descripton
 */

/*
 * @RunWith(SpringRunner.class)
 * 
 * @SpringBootTest
 * 
 * @WebAppConfiguration
 */
// @MapperScan("com.aixiao.seckill.mapper")
@Controller
public class OrderTest {

	@Autowired
	private OrderServiceImpl orderServiceImpl;

	@Autowired
	private UserServiceImpl userServiceImpl;
	@Autowired
	private SessionDAO sessionDAO;

	private static final Logger logger = LoggerFactory.getLogger(OrderTest.class);

	@Test
	public void test() {
		Order byId = orderServiceImpl.getById(9);
		System.out.println(byId);
	}

	@RequiresRoles("admin")
	@RequiresPermissions("create")
	@RequestMapping("/getUser")
	@ResponseBody
	public User getUser(User User) {
		User user = userServiceImpl.login(User);
		return user;
	}

	@RequestMapping("/login")
	@ResponseBody
	public JsonResult login(String username, String password, RedirectAttributes model) {
		// 想要得到 SecurityUtils.getSubject() 的对象．．访问地址必须跟 shiro 的拦截地址内．不然后会报空指针
		Subject sub = SecurityUtils.getSubject();
		// 用户输入的账号和密码,,存到UsernamePasswordToken对象中..然后由shiro内部认证对比,
		// 认证执行者交由 com.battcn.config.AuthRealm 中 doGetAuthenticationInfo 处理
		// 当以上认证成功后会向下执行,认证失败会抛出异常
		UsernamePasswordToken token = new UsernamePasswordToken(username, password, true);
		try {
			sub.login(token);
		} catch (UnknownAccountException e) {
			logger.error("对用户[{}]进行登录验证,验证未通过,用户不存在", username);
			token.clear();
			return new JsonResult(false, "UnknownAccountException");
		} catch (LockedAccountException lae) {
			logger.error("对用户[{}]进行登录验证,验证未通过,账户已锁定", username);
			token.clear();
			return new JsonResult(false, "LockedAccountException");
		} catch (ExcessiveAttemptsException e) {
			logger.error("对用户[{}]进行登录验证,验证未通过,错误次数过多", username);
			token.clear();
			return new JsonResult(false, "ExcessiveAttemptsException");
		} catch (AuthenticationException e) {
			logger.error("对用户[{}]进行登录验证,验证未通过,堆栈轨迹如下", username, e);
			token.clear();
			return new JsonResult(false, "AuthenticationException");
		}

		// 从session中获取 user 对象
		Session session = SecurityUtils.getSubject().getSession();
		User user = (User) session.getAttribute("USER_SESSION");

		return new JsonResult(true, "ok", user);
	}

	@RequestMapping("/show")
	@ResponseBody
	public JsonResult show() {
		Collection<Session> sessions = sessionDAO.getActiveSessions();
		for (Session session : sessions) {

			System.out.println("登录ip:" + session.getHost());

			System.out.println("登录用户" + session.getAttribute("USER_SESSION"));

			System.out.println("最后操作日期:" + session.getLastAccessTime());

		}

		return new JsonResult(true, "", sessions);
	}

	@RequestMapping("/denied")
	@ResponseBody
	public String denied() {
		return "权限不足！";
	}

	@RequestMapping("/licm/addMsg")
	@ResponseBody
	public JsonResult addMsg(Msg msgEntry) {
		try {
			Date date = new Date();
			msgEntry.setReplyTime(date);
			return new JsonResult(true, "保存成功！");
		} catch (Exception e) {
			logger.error("信息保存失败！" + e.getMessage());
			e.printStackTrace();
			return new JsonResult(false, "");
		}
	}

	@RequestMapping("/licm/getMsgById/{id}")
	@ResponseBody
	@RequiresPermissions(value = { "msg:find" })
	public JsonResult getMsgById(@PathVariable("id") String id) {
		try {
			return new JsonResult(true, "成功");
		} catch (Exception e) {
			logger.error("查询失败！");
			return new JsonResult(false, "");
		}
	}

	@RequestMapping("/licm/getMsgByPage/{start}/{size}")
	@ResponseBody
	@RequiresPermissions(value = { "msg:list" })
	public JsonResult getMsgById(@PathVariable("start") int start, @PathVariable("size") int size) {
		try {
			return new JsonResult(true, "成功");
		} catch (Exception e) {
			logger.error("查询失败！");
			return new JsonResult(false, "");
		}
	}

}

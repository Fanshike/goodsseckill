package com.aixiao.redis.util;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @author fan shi ke
 * @date 2019年3月26日 下午6:28:34
 * @Descripton
 */

@PropertySource("classpath:application.yml")
@ConfigurationProperties(prefix="spring.redis.pool") 
public class JedisUtil {
	
	 private Integer maxIdle;
	 private Integer minIdle;
	 private Integer maxActive;
	 private Integer maxWait;
	
	/**
	 * @return the maxIdle
	 */
	public Integer getMaxIdle() {
		return maxIdle;
	}

	/**
	 * @param maxIdle the maxIdle to set
	 */
	public void setMaxIdle(Integer maxIdle) {
		this.maxIdle = maxIdle;
	}

	/**
	 * @return the minIdle
	 */
	public Integer getMinIdle() {
		return minIdle;
	}

	/**
	 * @param minIdle the minIdle to set
	 */
	public void setMinIdle(Integer minIdle) {
		this.minIdle = minIdle;
	}

	/**
	 * @return the maxActive
	 */
	public Integer getMaxActive() {
		return maxActive;
	}

	/**
	 * @param maxActive the maxActive to set
	 */
	public void setMaxActive(Integer maxActive) {
		this.maxActive = maxActive;
	}

	/**
	 * @return the maxWait
	 */
	public Integer getMaxWait() {
		return maxWait;
	}

	/**
	 * @param maxWait the maxWait to set
	 */
	public void setMaxWait(Integer maxWait) {
		this.maxWait = maxWait;
	}

	/**
	 * @return the host
	 */


	@Bean
	public JedisPoolConfig getJedisPoolConfig(){
		System.out.println("maxIdle:"+maxIdle+" minidle:"+minIdle);
		JedisPoolConfig config = new JedisPoolConfig();
		config.setMaxIdle(maxIdle);
		config.setMinIdle(minIdle);
		config.setMaxTotal(maxActive);
		config.setMaxWaitMillis(maxWait);
		return config;
	}
	
	@Bean
	public JedisPool getJedisPool() {
		return new JedisPool(getJedisPoolConfig(),"192.168.145.128",6379);
	}

	@Bean
	public Jedis getJedis() {
			return getJedisPool().getResource();
	}
}

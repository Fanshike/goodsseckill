package com.aixiao.shiro.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.aixiao.entity.seckill.User;
import com.aixiao.seckill.service.impl.PermissionServiceImpl;
import com.aixiao.seckill.service.impl.RoleServiceImpl;
import com.aixiao.seckill.service.impl.UserServiceImpl;

/**
 * 自定义 realm
 */
public class MyRealm extends AuthorizingRealm {
	
	public static final Logger logger = org.slf4j.LoggerFactory.getLogger(MyRealm.class);
	
	@Autowired
	private UserServiceImpl userServiceimimImpl;

	@Autowired
	private RoleServiceImpl roleServiceImpl;

	@Autowired
	private PermissionServiceImpl permissionServiceImpl;

	@Autowired
	private SessionDAO sessionDAO;

	@Override
	public String getName() {
		return "myRealm";
	}

	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
		// 从session中获取 user 对象
		Session session = SecurityUtils.getSubject().getSession();
		User user = (User) session.getAttribute("USER_SESSION");
		// 权限信息对象
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		List<Map<String, Object>> byUserId = roleServiceImpl.getByUserId(user.getId());
		List<String> roleList = new ArrayList<String>();
		List<String> perList = new ArrayList<String>();
		for (int i = 0; i < byUserId.size(); i++) {
			roleList.add((String) byUserId.get(i).get("roleName"));
			List<Map<String, Object>> byRoleId = permissionServiceImpl.getByRoleId((Integer) byUserId.get(i).get("id"));
			for (int j = 0; j < byRoleId.size(); j++) {
				perList.add((String) byRoleId.get(j).get("permission"));
			}
		}

		info.addRoles(roleList);
		info.addStringPermissions(perList);

		return info;
	}

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken)
			throws AuthenticationException {
		// 加这一步的目的是在Post请求的时候会先进认证，然后在到请求
		if (authenticationToken.getPrincipal() == null) {
			return null;
		}
		String username = (String) authenticationToken.getPrincipal();
		String password = new String((char[]) authenticationToken.getCredentials());
		logger.warn("username=" + username + ",password=" + password);
		// 获取用户名。通过 username 找到该用户
		User user = userServiceimimImpl.findByName(username);
		if ("1".equals(user.getStutas())) {
			throw new LockedAccountException();
		}
		if (username.equals(user.getName()) && password.equals(user.getPassWord())) {
			// 获取所有session
			Collection<Session> sessions = sessionDAO.getActiveSessions();
			for (Session session : sessions) {
				User sysUser = (User) session.getAttribute("USER_SESSION");
				// 如果session里面有当前登陆的，则证明是重复登陆的，则将其剔除
				if (sysUser!= null && sysUser.getName().equals(user.getName())) {
					if (username.equals(sysUser.getName())) {
						session.setTimeout(0);
					}
				}
			}
			// 验证通过了 setSession
			Session session = SecurityUtils.getSubject().getSession();
			session.setAttribute("USER_SESSION", user);

		}

		// 从数据库查询出来的用户名密码，进行验证
		// 用户名，密码，密码盐值，realm 名称
		// 登陆的时候直接调用 subject.login() 即可自动调用该方法
		SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(authenticationToken.getPrincipal(),
				user.getPassWord(), getName());

		return info;
	}
}

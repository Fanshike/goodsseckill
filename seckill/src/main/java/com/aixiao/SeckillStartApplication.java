package com.aixiao;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * @author fan shi ke
 * @date 2019年3月20日 下午5:47:33
 * @Descripton
 */

@SpringBootApplication
@ServletComponentScan 
@MapperScan("com.aixiao.seckill.mapper") 
public class SeckillStartApplication {
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(SeckillStartApplication.class, args);
	}

}

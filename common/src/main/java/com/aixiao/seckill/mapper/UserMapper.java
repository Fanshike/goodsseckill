package com.aixiao.seckill.mapper;

import java.util.List;
import java.util.Map;

import com.aixiao.entity.seckill.User;

/**
 * @author fan shi ke 
 * @date 2019年3月20日 下午6:41:29
 * @Descripton
 */
public interface UserMapper {
	
	public List<Map<String,Object>> queryAll();
	
	public User getById(Integer id);
	
	public User getByName(String name);
	
	public User getUserByNameAndPassWord(User user);
}

package com.aixiao.seckill.mapper;

import java.util.List;
import java.util.Map;

import com.aixiao.entity.seckill.Goods;

/**
 * @author fan shi ke 
 * @date 2019年3月20日 下午6:41:29
 * @Descripton
 */
public interface GoodsMapper {
	
	public List<Map<String,Object>> queryAll(Integer pageIndex);
	
	public Long getAllSize();
	
	public Goods getById(Integer id);
}

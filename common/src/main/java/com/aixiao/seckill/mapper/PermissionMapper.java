package com.aixiao.seckill.mapper;

import java.util.List;
import java.util.Map;

import com.aixiao.entity.seckill.Permission;

/**
 * @author fan shi ke 
 * @date 2019年3月20日 下午6:41:29
 * @Descripton
 */
public interface PermissionMapper {
	
	public List<Map<String,Object>> queryAll(Integer pageIndex);
	
	public Long getAllSize();
	
	public Permission getById(Integer id);
	
	public List<Map<String,Object>> getByRoleId(Integer roleId);

}

package com.aixiao.seckill.service;

import java.util.List;
import java.util.Map;

import com.aixiao.entity.seckill.Role;


/**
 * @author fan shi ke 
 * @date 2019年3月20日 下午5:26:59
 * @Descripton
 */
public interface RoleService {
	
	public List<Map<String,Object>> queryAll(Integer page);
	
	public Long getAllSize();
	
	public Role getById(Integer id);
	
	public List<Map<String,Object>> getByUserId(Integer userId);

}

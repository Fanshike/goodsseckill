package com.aixiao.seckill.service;

import java.util.List;
import java.util.Map;

import com.aixiao.entity.seckill.User;

/**
 * @author fan shi ke 
 * @date 2019年3月20日 下午5:26:59
 * @Descripton
 */
public interface UserService {
	
	public User getById(Integer id);
	
	public List<Map<String,Object>> queryAll();
	
	public User login(User user);
	
	public User findByName(String name);
	
}

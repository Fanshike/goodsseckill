package com.aixiao.seckill.service;

import java.util.List;
import java.util.Map;

import com.aixiao.entity.seckill.Goods;

/**
 * @author fan shi ke 
 * @date 2019年3月20日 下午5:26:59
 * @Descripton
 */
public interface GoodsService {
	
	public List<Map<String, Object>> queryAll(Integer page);
	
	public Long getAllSize();
	
	public Goods getById(Integer id);
}

package com.aixiao.seckill.service;

import java.util.List;
import java.util.Map;

import com.aixiao.entity.seckill.Permission;


/**
 * @author fan shi ke 
 * @date 2019年3月20日 下午5:26:59
 * @Descripton
 */
public interface PermissionService {
	
	public List<Map<String,Object>> queryAll(Integer page);
	
	public Long getAllSize();
	
	public Permission getById(Integer id);
	
	public List<Map<String,Object>> getByRoleId(Integer roleId);

}

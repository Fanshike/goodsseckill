package com.aixiao.entity.seckill;

/**
 * @author fan shi ke
 * @date 2019年3月25日 下午3:57:53
 * @Descripton
 */
public class Role {

	private Long id;
	private String roleName;
	private Integer userId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	

	
}

package com.aixiao.entity.seckill;

import java.util.Date;


/**
 * @author fan shi ke 
 * @date 2019年3月20日 下午5:05:25
 * @Descripton
 */
public class Goods {
	
	private Integer id;
	private String name;
	private Double money;
	private Date  startTime;
	private Date  endTime;
	private int num;
	public Goods() {
	}
	public Goods(String name, Double money, Date  startTime, Date  endTime, int num) {
		super();
		this.name = name;
		this.money = money;
		this.startTime = startTime;
		this.endTime = endTime;
		this.num = num;
	}
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the money
	 */
	public Double getMoney() {
		return money;
	}
	/**
	 * @param money the money to set
	 */
	public void setMoney(Double money) {
		this.money = money;
	}
	/**
	 * @return the startTime
	 */
	public Date  getStartTime() {
		return startTime;
	}
	public void setStartTime(Date  startTime) {
		this.startTime = startTime;
	}
	/**
	 * @return the endTime
	 */
	public Date  getEndTime() {
		return endTime;
	}
	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(Date  endTime) {
		this.endTime = endTime;
	}
	/**
	 * @return the num
	 */
	public int getNum() {
		return num;
	}
	/**
	 * @param num the num to set
	 */
	public void setNum(int num) {
		this.num = num;
	}
	
	
}
